#Copyright (c) 2009 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# variant and related types

from ctypes import Structure, c_void_p, windll, POINTER, HRESULT, byref
from windef import USHORT, WORD
from winliteutils import get_symbols

VARTYPE = USHORT

VT_EMPTY = 0
VT_NULL = 1
VT_I2 = 2
VT_I4 = 3
VT_R4 = 4
VT_R8 = 5
VT_CY = 6
VT_DATE = 7
VT_BSTR = 8
VT_DISPATCH = 9
VT_ERROR = 10
VT_BOOL = 11
VT_VARIANT = 12
VT_UNKNOWN = 13
VT_DECIMAL = 14
VT_I1 = 16
VT_UI1 = 17
VT_UI2 = 18
VT_UI4 = 19
VT_I8 = 20
VT_UI8 = 21
VT_INT = 22
VT_UINT = 23
VT_VOID = 24
VT_HRESULT = 25
VT_PTR = 26
VT_SAFEARRAY = 27
VT_CARRAY = 28
VT_USERDEFINED = 29
VT_LPSTR = 30
VT_LPWSTR = 31
VT_RECORD = 36
VT_INT_PTR = 37
VT_UINT_PTR = 38
VT_FILETIME = 64
VT_BLOB = 65
VT_STREAM = 66
VT_STORAGE = 67
VT_STREAMED_OBJECT = 68
VT_STORED_OBJECT = 69
VT_BLOB_OBJECT = 70
VT_CF = 71
VT_CLSID = 72
VT_VERSIONED_STREAM = 73
VT_BSTR_BLOB = 0x0fff
VT_VECTOR = 0x1000
VT_ARRAY = 0x2000
VT_BYREF = 0x4000
VT_RESERVED = 0x8000
VT_ILLEGAL = 0xffff
VT_ILLEGALMASKED = 0x0fff
VT_TYPEMASK = 0x0fff

_oleaut32 = windll.oleaut32
get_symbols(globals(), _oleaut32, ['VariantClear', 'VariantInit'])

class _VARIANT_VAL(Structure):
    _fields_ = [('p1', c_void_p),
                ('p2', c_void_p),
                ]

class VARIANT(Structure):
    _fields_ = [('vt', VARTYPE),
                ('wReserved1', WORD),
                ('wReserved2', WORD),
                ('wReserved3', WORD),
                ('_value', _VARIANT_VAL),
                ]

    def __init__(self):
        VariantInit(byref(self))

    def _set_value(self, value):
        try:
            setter = value.__setvariant__
        except AttributeError:
            try:
                setter = _variant_setters[type(value)]
            except KeyError:
                raise TypeError("variable of type %s cannot be converted to variant" % str(type(value)))
            else:
                setter[0](self, value, *setter[1:])
        else:
            setter(self)

    def _get_value(self):
        try:
            getter = _variant_getters[self.vt]
        except KeyError:
            raise TypeError("getting variant type %s not supported" % self.vt)
        else:
            return getter[0](self, *getter[1:])

    value = property(_get_value, _set_value)

    def __del__(self):
        VariantClear(byref(self))

VARIANTARG = VARIANT
LPVARIANTARG = LPVARIANT = POINTER(VARIANT)

VariantClear.argtypes = [LPVARIANTARG]
VariantClear.restype = HRESULT

VariantInit.argtypes = [LPVARIANTARG]
VariantInit.restype = None

def _get_empty(variant):
    return None

def _set_empty(variant, value):
    VariantClear(byref(var))

class _SQL_NULL(object):
    def __setvariant__(self, var):
        VariantClear(byref(var))
        var.vt = VT_NULL

sql_null = _SQL_NULL()

def _get_null(variant):
    return sql_null

_variant_getters = {
    VT_EMPTY: (_get_empty,),
    VT_NULL: (_get_null,),
    }

_variant_setters = {
    type(None): (_set_empty,),
    }

