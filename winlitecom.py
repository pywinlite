#Copyright (c) 2009 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# basic COM utilities

from ctypes import POINTER, c_void_p, Structure, WINFUNCTYPE, cast, byref, addressof
from windef import HRESULT, GUID, REFIID, ULONG

class _COMMethod(object):
    __slots__ = ['p', 'name']
    def __init__(self, obj, name):
        self.p = obj.p
        self.name = name

    def __call__(self, *args):
        return getattr(self.p.contents.contents, self.name)(self.p, *args)

class _COMMethodGetter(object):
    __slots__ = ['name']
    def __init__(self, name):
        self.name = name

    def __get__(self, instance, owner):
        return _COMMethod(instance, self.name)

def _translate_methods(methods, vtable):
    # translates a com method list into a vtable
    result = []
    objtype = POINTER(POINTER(vtable))
    for method in methods:
        name = method[0]
        functype = WINFUNCTYPE(method[1], objtype, *method[2:])
        result.append((name, functype))
    return result

class _interface_outparam(object):
    __slots__ = ['p', '_as_parameter_']
    def __init__(self):
        self.p = c_void_p(0)
        self._as_parameter_ = addressof(self.p)

    @classmethod
    def from_param(cls, arg):
        if arg == None:
            return c_void_p(0)
        elif isinstance(arg, cls) or (isinstance(arg, _interface_outparam) and issubclass(cls.cls, arg.cls)):
            return arg
        else:
            raise TypeError

    def get(self):
        if self.p.value:
            result = self.cls(self.p.value)
            self.p.value = None
            return result
        else:
            raise ValueError("NULL interface pointer")

    def __del__(self):
        if self.p.value:
            self.get()

class _InterfaceClass(type):
    def __init__(self, name, bases, dict):
        type.__init__(self, name, bases, dict)

        # create the vtable type
        class vtable(Structure):
            pass
        vtable.__name__ = '_%s_vtable' % name
        self._vtable = vtable

        if 'com_methods' in dict:
            self.set_methods(self.com_methods)
        #else the creator of the class must call set_methods

        # a COM object is a pointer to a pointer to a vtable
        self.objtype = POINTER(POINTER(vtable))

        self.outparam = type('_%s_outparam' % name, (_interface_outparam,), {'cls': self})

    def set_methods(self, methods):
        # add the base class methods to our list of methods
        for base in self.__bases__:
            try:
                self.com_methods = base.com_methods + methods
                break
            except AttributeError:
                pass
        else:
            self.com_methods = methods

        self._vtable._fields_ = _translate_methods(self.com_methods, self._vtable)

        # add an attribute for each method
        for method in self.com_methods:
            setattr(self, method[0], _COMMethodGetter(method[0]))

class _BaseInterface(object):
    com_methods = []

    def __init__(self, p):
        self.p = cast(c_void_p(p), self.objtype)

    def _get_parameter(self):
        return self.p

    _as_parameter_ = property(_get_parameter)

    @classmethod
    def from_param(cls, arg):
        if arg is None:
            return c_void_p(0)
        elif isinstance(arg, cls):
            return arg
        else:
            raise TypeError

# Needed because 3.0 added new syntax for metaclasses and removed the old syntax.
_InterfaceClassInstanceBase = _InterfaceClass('_InterfaceClassInstanceBase', (_BaseInterface,), {})

class IUnknown(_InterfaceClassInstanceBase):
    com_methods = [
        ('QueryInterface', HRESULT, REFIID, POINTER(c_void_p)),
        ('AddRef', ULONG),
        ('Release', ULONG),
        ]

    iid = GUID("00000000-0000-0000-C000-000000000046")

    def __init__(self, obj):
        try:
            p = c_void_p()
            obj.QueryInterface(byref(self.iid), byref(p))
            self.p = cast(p, self.objtype)
        except AttributeError:
            _InterfaceClassInstanceBase.__init__(self, obj)

    def __del__(self):
        self.Release()

