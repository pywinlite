#Copyright (c) 2009 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# ole pictures

from ctypes import POINTER, windll, byref

from windef import LONG, GUID, HRESULT, OLE_HANDLE, SHORT, HDC, LPCRECT, BOOL, LPVOID, REFIID, DWORD
from winliteutils import get_symbols
from winlitecom import IUnknown
from storage import IStream, file_stream, STGM_SHARE_DENY_WRITE, STGM_READ

OLE_XPOS_HIMETRIC = LONG
OLE_YPOS_HIMETRIC = LONG
OLE_XSIZE_HIMETRIC = LONG
OLE_YSIZE_HIMETRIC = LONG

class IPicture(IUnknown):
    iid = GUID("7bf80980-bf32-101a-8bbb-00aa00300cab")

    com_methods = [('get_Handle', HRESULT, POINTER(OLE_HANDLE)),
                   ('get_hPal', HRESULT, POINTER(OLE_HANDLE)),
                   ('get_Type', HRESULT, POINTER(SHORT)),
                   ('get_Width', HRESULT, POINTER(OLE_XSIZE_HIMETRIC)),
                   ('get_Height', HRESULT, POINTER(OLE_YSIZE_HIMETRIC)),
                   ('Render', HRESULT, HDC, LONG, LONG, LONG, LONG, OLE_XPOS_HIMETRIC, OLE_YPOS_HIMETRIC, OLE_XSIZE_HIMETRIC, OLE_YSIZE_HIMETRIC, LPCRECT),
                   ('set_hPal', HRESULT, POINTER(OLE_HANDLE)),
                   ('get_CurDC', HRESULT, HDC),
                   ('SelectPicture', HRESULT, HDC, POINTER(HDC), POINTER(OLE_HANDLE)),
                   ('get_KeepOriginalFormat', HRESULT, POINTER(BOOL)),
                   ('put_KeepOriginalFormat', HRESULT, BOOL),
                   ('PictureChanged', HRESULT),
                   ('SaveAsFile', HRESULT, IStream, BOOL, LONG),
                   ('get_Attributes', HRESULT, POINTER(DWORD)),
                   ]

    def _get_width(self):
        result = OLE_XSIZE_HIMETRIC()
        self.get_Width(byref(result))
        return result.value

    width = property(_get_width)

    def _get_height(self):
        result = OLE_YSIZE_HIMETRIC()
        self.get_Height(byref(result))
        return result.value

    height = property(_get_height)

    def _get_handle(self):
        result = OLE_HANDLE()
        self.get_Handle(byref(result))
        return result.value

    handle = property(_get_handle)

_oleaut32 = windll.oleaut32

get_symbols(globals(), _oleaut32, ['OleLoadPicture'])

OleLoadPicture.argtypes = [IStream, LONG, BOOL, REFIID, LPVOID]
OleLoadPicture.restype = HRESULT

def stream_picture(stream):
    p = LPVOID()
    OleLoadPicture(stream, 0, 1, byref(IPicture.iid), byref(p))
    return IPicture(p.value)

def file_picture(filename):
    stream = file_stream(filename, STGM_SHARE_DENY_WRITE|STGM_READ)
    return stream_picture(stream)

