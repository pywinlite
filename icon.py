#Copyright (c) 2008 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# Icon definitions
# see http://msdn.microsoft.com/en-us/library/ms646973(VS.85).aspx

from windef import HICON, HINSTANCE, INT, BYTE, LPBYTE, PBYTE, BOOL, UINT
from winliteutils import NONZERO
from resource import MAKEINTRESOURCE

IDI_APPLICATION = MAKEINTRESOURCE(32512)
IDI_HAND = MAKEINTRESOURCE(32513)
IDI_QUESTION = MAKEINTRESOURCE(32514)
IDI_EXCLAMATION = MAKEINTRESOURCE(32515)
IDI_ASTERISK = MAKEINTRESOURCE(32516)
IDI_WINLOGO = MAKEINTRESOURCE(32517)

IDI_WARNING = IDI_EXCLAMATION
IDI_ERROR = IDI_HAND
IDI_INFORMATION = IDI_ASTERISK

_user32.CopyIcon.argtypes = [HICON]
_user32.CopyIcon.restype = NONZERO
CopyIcon = _user32.CopyIcon

_user32.CreateIcon.argtypes = [HINSTANCE, INT, INT, BYTE, BYTE, LPBYTE, LPBYTE]
_user32.CreateIcon.restype = NONZERO
CreateIcon = _user32.CreateIcon

_user32.CreateIconFromResource.argtypes = [PBYTE, DWORD, BOOL, DWORD]
_user32.CreateIconFromResource.restype = NONZERO
CreateIconFromResource = _user32.CreateIconFromResource

_user32.CreateIconFromResourceEx.argtypes = [PBYTE, DWORD, BOOL, DWORD, INT, INT, UINT]
_user32.CreateIconFromResourceEx.restype = 

