#Copyright (c) 2008 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# Resource definitions
# see http://msdn.microsoft.com/en-us/library/ms632583(VS.85).aspx

from ctypes import c_void_p, cast, windll
from winlitecfg import get_aw_symbols
from winliteutils import NONZERO
from windef import LPCTSTR, BOOL, HANDLE, UINT, INT, WINFUNCTYPE, WORD, LONG_PTR, DWORD, LPTSTR, HGLOBAL, HMODULE, HINSTANCE, HRSRC
_kernel32 = windll.kernel32
_user32 = windll.user32

# TODO: define Enum* functions and resource-updating functions

def MAKEINTRESOURCE(i):
    return cast(c_void_p(i&0xFFFF), LPCTSTR)

def IS_INTRESOURCE(i):
    try:
        return i.value|0xFFFF == 0xFFFF
    except AttributeError:
        return i|0xFFFF == 0xFFFF

IMAGE_BITMAP = 0
IMAGE_ICON = 1
IMAGE_CURSOR = 2
IMAGE_ENHMETAFILE = 3

LR_DEFAULTCOLOR = 0x0000
LR_MONOCHROME = 0x0001
LR_COLOR = 0x0002
LR_COPYRETURNORG = 0x0004
LR_COPYDELETEORG = 0x0008
LR_LOADFROMFILE = 0x0010
LR_LOADTRANSPARENT = 0x0020
LR_DEFAULTSIZE = 0x0040
LR_VGA_COLOR = 0x0080
LR_LOADMAP3DCOLORS = 0x1000
LR_CREATEDIBSECTION = 0x2000
LR_COPYFROMRESOURCE = 0x4000
LR_SHARED = 0x8000

RT_CURSOR = MAKEINTRESOURCE(1)
RT_BITMAP = MAKEINTRESOURCE(2)
RT_ICON = MAKEINTRESOURCE(3)
RT_MENU = MAKEINTRESOURCE(4)
RT_DIALOG = MAKEINTRESOURCE(5)
RT_STRING = MAKEINTRESOURCE(6)
RT_FONTDIR = MAKEINTRESOURCE(7)
RT_FONT = MAKEINTRESOURCE(8)
RT_ACCELERATOR = MAKEINTRESOURCE(9)
RT_RCDATA = MAKEINTRESOURCE(10)
RT_MESSAGETABLE = MAKEINTRESOURCE(11)
RT_GROUP_CURSOR = MAKEINTRESOURCE(12)
RT_GROUP_ICON = MAKEINTRESOURCE(14)
RT_VERSION = MAKEINTRESOURCE(16)
RT_DLGINCLUDE = MAKEINTRESOURCE(17)
RT_PLUGPLAY = MAKEINTRESOURCE(19)
RT_VXD = MAKEINTRESOURCE(20)
RT_ANICURSOR = MAKEINTRESOURCE(21)
RT_ANIICON = MAKEINTRESOURCE(22)
RT_HTML = MAKEINTRESOURCE(23)
RT_MANIFEST = MAKEINTRESOURCE(24)

get_aw_symbols(globals(), _kernel32, ['FindResource', 'FindResourceEx'])
get_aw_symbols(globals(), _user32, ['LoadImage'])

_user32.CopyImage.argtypes = [HANDLE, UINT, INT, INT, UINT]
_user32.CopyImage.restype = NONZERO
CopyImage = _user32.CopyImage

FindResource.argtypes = [HMODULE, LPCTSTR, LPCTSTR]
FindResource.restype = NONZERO

FindResourceEx.argtypes = [HMODULE, LPCTSTR, LPCTSTR, WORD]
FindResourceEx.restype = NONZERO

_kernel32.FreeResource.argtypes = [HGLOBAL]
_kernel32.FreeResource.restype = BOOL
FreeResource = _kernel32.FreeResource

LoadImage.argtypes = [HINSTANCE, LPCTSTR, UINT, INT, INT, UINT]
LoadImage.restype = NONZERO

_kernel32.LoadResource.argtypes = [HMODULE, HRSRC]
_kernel32.LoadResource.restype = NONZERO
LoadResource = _kernel32.LoadResource

_kernel32.LockResource.argtypes = [HGLOBAL]
_kernel32.LockResource.restype = NONZERO
LockResource = _kernel32.LockResource

_kernel32.SizeofResource.argtypes = [HMODULE, HRSRC]
_kernel32.SizeofResource.restype = NONZERO
SizeofResource = _kernel32.SizeofResource

