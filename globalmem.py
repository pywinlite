#Copyright (c) 2008 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# Global memory definitions
# see http://msdn.microsoft.com/en-us/library/aa366596(VS.85).aspx

from ctypes import windll, WinError
from winliteutils import get_symbols, NONZERO, ZERO
from windef import UINT, SIZE_T, HGLOBAL, LPCVOID, LPVOID

_kernel32 = windll.kernel32

GMEM_FIXED = 0x0000
GMEM_MOVEABLE = 0x0002
GMEM_NOCOMPACT = 0x0010
GMEM_NODISCARD = 0x0020
GMEM_ZEROINIT = 0x0040
GMEM_MODIFY = 0x0080
GMEM_DISCARDABLE = 0x0100
GMEM_NOT_BANKED = 0x1000
GMEM_SHARE = 0x2000
GMEM_DDESHARE = 0x2000
GMEM_NOTIFY = 0x4000
GMEM_LOWER = GMEM_NOT_BANKED
GMEM_DISCARDED = 0x4000
GMEM_LOCKCOUNT = 0x00ff
GMEM_INVALID_HANDLE = 0x8000

GHND = (GMEM_MOVEABLE | GMEM_ZEROINIT)
GPTR = (GMEM_FIXED | GMEM_ZEROINIT)

def gmem_handle(x):
    if x == GMEM_INVALID_HANDLE:
        raise WinError()
    return x

get_symbols(globals(), _kernel32, ['GlobalAlloc', 'GlobalFlags', 'GlobalFree', 'GlobalHandle', 'GlobalLock', 'GlobalReAlloc', 'GlobalSize', 'GlobalUnlock'])

GlobalAlloc.argtypes = [UINT, SIZE_T]
GlobalAlloc.restype = NONZERO

GlobalFlags.argtypes = [HGLOBAL]
GlobalFlags.restype = gmem_handle

GlobalFree.argtypes = [HGLOBAL]
GlobalFree.restype = ZERO

GlobalHandle.argtypes = [LPCVOID]
GlobalHandle.restype = NONZERO

GlobalLock.argtypes = [HGLOBAL]
GlobalLock.restype = NONZERO

GlobalReAlloc.argtypes = [HGLOBAL, SIZE_T, UINT]
GlobalReAlloc.restype = NONZERO

GlobalSize.argtypes = [HGLOBAL]
GlobalSize.restype = NONZERO

GlobalUnlock.argtypes = [HGLOBAL]
GlobalUnlock.restype = NONZERO

#obsolete:
#GlobalLRUNewest = lambda x: x
#GlobalLRUOldest = lambda x: x

def GlobalDiscard(handle):
    return GlobalReAlloc(handle, 0, GMEM_MOVEABLE)

class GlobalMemoryObject(object):
    __slots__ = ['handle', 'needs_free', '_as_parameter_']
    
    def __init__(self, flags=GMEM_MOVEABLE, size=0, handle=None, needs_free=True):
        if handle is None:
            handle = GlobalAlloc(flags, size)
        self.handle = self._as_parameter_ = handle
        self.needs_free = needs_free
    
    def free(self):
        if self.needs_free:
            if GlobalFree(self) != 0:
                raise WinError()
            self.needs_free = False
            self.handle = self._as_parameter_ = 0
    
    def lock(self):
        return GlobalLock(self)
    
    def unlock(self):
        GlobalUnlock(self)
    
    def __enter__(self):
        return self.lock()
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.unlock()
    
    def realloc(self, size, flags=GMEM_MOVEABLE):
        GlobalReAlloc(self, size, flags)
    
    def discard(self):
        GlobalDiscard(self)
    
    def get_flags(self):
        return GlobalFlags(self)
    
    def get_size(self):
        return GlobalSize(self)
    
    def __del__(self):
        self.free()

