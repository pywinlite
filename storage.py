#Copyright (c) 2009 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# structured storage

from ctypes import POINTER, windll, c_void_p, Structure, byref, cast, create_string_buffer, c_uint32, Union, addressof
from winlitecfg import get_aw_symbols
from windef import HRESULT, DWORD, LPCWSTR, LPVOID, GUID, LPCOLESTR, ULONG, PULONG, LPOLESTR, ULARGE_INTEGER, FILETIME, CLSID, S_OK, REFCLSID, LARGE_INTEGER, UINT, USHORT, REFIID, LPCTSTR, FMTID, REFFMTID, WORD, LPSTR, LONG
from winliteutils import get_symbols
from winlitecom import IUnknown

class SNB(object):
    @classmethod
    def from_param(cls, arg):
        if arg == None:
            return c_void_p(0)
        else:
            # TODO
            raise NotImplemented

try:
    _bytes = bytes
except NameError:
    _bytes = str

class ISequentialStream(IUnknown):
    com_methods = [('Read', HRESULT, LPVOID, ULONG, PULONG),
                   #RemoteRead = Read
                   ('Write', HRESULT, LPVOID, ULONG, PULONG),
                   #RemoteWrite = Write
                   ]

    iid = GUID("0c733a30-2a1c-11ce-ade5-00aa0044773d")

    def read(self, size=None):
        if size:
            result = create_string_buffer(size)
            bytesread = ULONG()
            self.Read(cast(result, LPVOID), size, byref(bytesread))
            return result.raw[0:bytesread.value]
        else:
            blocks = []
            result = create_string_buffer(1024)
            bytesread = ULONG(1024)
            while bytesread.value == 1024:
                self.Read(cast(result, LPVOID), 1024, byref(bytesread))
                block = result.raw[0:bytesread.value]
                blocks.append(block)
            return _bytes().join(blocks)

STGTY_STORAGE = 1
STGTY_STREAM = 2
STGTY_LOCKBYTES = 3
STGTY_PROPERTY = 4

class STATSTG(Structure):
    _fields_ = [('pwcsName', LPOLESTR),
                ('type', DWORD),
                ('cbSize', ULARGE_INTEGER),
                ('mtime', FILETIME),
                ('ctime', FILETIME),
                ('atime', FILETIME),
                ('grfMode', DWORD),
                ('grfLocksSupported', DWORD),
                ('clsid', CLSID),
                ('grfStateBits', DWORD),
                ('reserved', DWORD),
                ]

    owns_name = False

    def __del__(self):
        if self.owns_name:
            _ole32.CoTaskMemFree(cast(self.pwcsName, c_void_p))
            self.owns_name = False

STREAM_SEEK_SET = 0
STREAM_SEEK_CUR = 1
STREAM_SEEK_END = 2

class IStream(ISequentialStream):
    iid = GUID("0000000c-0000-0000-C000-000000000046")

    def stat(self, mode):
        result = STATSTG()
        self.Stat(byref(result), mode)
        result.owns_name = True
        return result

    def seek(self, offset, origin):
        result = ULARGE_INTEGER()
        self.Seek(offset, origin, byref(result))
        return result.value

IStream.set_methods([('Seek', HRESULT, LARGE_INTEGER, DWORD, POINTER(ULARGE_INTEGER)),
                     #RemoteSeek = Seek
                     ('SetSize', HRESULT, ULARGE_INTEGER),
                     ('CopyTo', HRESULT, IStream, ULARGE_INTEGER, ULARGE_INTEGER, ULARGE_INTEGER),
                     #RemoteCopyTo = CopyTo
                     ('Commit', HRESULT, DWORD),
                     ('Revert', HRESULT),
                     ('LockRegion', HRESULT, ULARGE_INTEGER, ULARGE_INTEGER, DWORD),
                     ('UnlockRegion', HRESULT, ULARGE_INTEGER, ULARGE_INTEGER, DWORD),
                     ('Stat', HRESULT, POINTER(STATSTG), DWORD),
                     ('Clone', HRESULT, IStream.outparam),
                     ])

LPSTREAM = IStream

class IEnumSTATSTG(IUnknown):
    iid = GUID("0000000d-0000-0000-C000-000000000046")

    def __iter__(self):
        return self
    
    def __next__(self):
        result = STATSTG()
        numreturned = ULONG()
        if self.Next(1, byref(result), byref(numreturned)) == S_OK and numreturned.value == 1:
            result.owns_name = True
            return result
        else:
            raise StopIteration

    next = __next__


IEnumSTATSTG.set_methods([('Next', HRESULT, ULONG, POINTER(STATSTG), POINTER(ULONG)),
                          ('Skip', HRESULT, ULONG),
                          ('Reset', HRESULT),
                          ('Clone', HRESULT, IEnumSTATSTG.outparam),
                          ])

STATFLAG_DEFAULT = 0
STATFLAG_NONAME = 1
STATFLAG_NOOPEN = 2

class IStorage(IUnknown):
    iid = GUID("0000000b-0000-0000-C000-000000000046")

    def __iter__(self):
        result = IEnumSTATSTG.outparam()
        self.EnumElements(0, None, 0, result)
        return iter(result.get())

    def stat(self, mode):
        result = STATSTG()
        self.Stat(byref(result), mode)
        result.owns_name = True
        return result

    def open_storage(self, name, mode):
        result = IStorage.outparam()
        self.OpenStorage(name, None, mode, None, 0, result)
        return result.get()

    def open_stream(self, name, mode):
        result = IStream.outparam()
        self.OpenStream(name, None, mode, 0, result)
        return result.get()

    def create_stream(self, name, mode):
        result = IStream.outparam()
        self.CreateStream(name, mode, 0, 0, result)
        return result.get()

IStorage.set_methods([('CreateStream', HRESULT, LPCOLESTR, DWORD, DWORD, DWORD, IStream.outparam),
                      ('OpenStream', HRESULT, LPCOLESTR, LPVOID, DWORD, DWORD, IStream.outparam),
                      #RemoteOpenStream = OpenStream
                      ('CreateStorage', HRESULT, LPCOLESTR, DWORD, DWORD, DWORD, IStorage.outparam),
                      ('OpenStorage', HRESULT, LPCOLESTR, IStorage, DWORD, SNB, DWORD, IStorage.outparam),
                      ('CopyTo', HRESULT, DWORD, POINTER(GUID), SNB, IStorage),
                      ('MoveElementTo', HRESULT, LPCOLESTR, IStorage, LPCOLESTR, DWORD),
                      ('Commit', HRESULT, DWORD),
                      ('Revert', HRESULT),
                      ('EnumElements', HRESULT, DWORD, LPVOID, DWORD, IEnumSTATSTG.outparam),
                      #RemoteEnumElements = EnumElements
                      ('DestroyElement', HRESULT, LPCOLESTR),
                      ('RenameElement', HRESULT, LPCOLESTR, LPCOLESTR),
                      ('SetElementTimes', HRESULT, LPCOLESTR, POINTER(FILETIME), POINTER(FILETIME), POINTER(FILETIME)),
                      ('SetClass', HRESULT, REFCLSID),
                      ('SetStateBits', HRESULT, DWORD, DWORD),
                      ('Stat', HRESULT, POINTER(STATSTG), DWORD),
                      ])

STGM_DIRECT = 0x0
STGM_FAILIFTHERE = 0x0
STGM_READ = 0x0
STGM_WRITE = 0x1
STGM_READWRITE = 0x2
STGM_SHARE_EXCLUSIVE = 0x10
STGM_SHARE_DENY_WRITE = 0x20
STGM_SHARE_DENY_READ = 0x30
STGM_SHARE_DENY_NONE = 0x40
STGM_CREATE = 0x1000
STGM_TRANSACTED = 0x10000
STGM_CONVERT = 0x20000
STGM_PRIORITY = 0x40000
STGM_NOSCRATCH = 0x100000
STGM_NOSNAPSHOT = 0x200000
STGM_DIRECT_SWMR = 0x400000
STGM_DELETEONRELEASE = 0x4000000
STGM_SIMPLE = 0x8000000

STGFMT = UINT
STGFMT_STORAGE = 0
STGFMT_FILE = 3
STGFMT_ANY = 4
STGFMT_DOCFILE = 5

PROPID = ULONG

PRSPEC_INVALID = 0xffffffff
PRSPEC_LPWSTR = 0
PRSPEC_PROPID = 1

class PROPSPEC(Structure):
    class U(Union):
        _fields_ = [('propid', PROPID),
                    ('lpwstr', LPOLESTR),
                    ]
    _fields_ = [('ulKind', ULONG),
                ('u', U),
                ]

VARTYPE = USHORT

VT_I4 = 3
VT_LPSTR = 30
VT_FILETIME = 64

def _get_propvar_i4(value):
    return cast(addressof(value), POINTER(LONG)).contents.value

def _get_propvar_lpstr(value):
    return cast(addressof(value), POINTER(LPSTR)).contents.value

def _get_propvar_filetime(value):
    return cast(addressof(value), POINTER(FILETIME)).contents

_propvar_getters = {
    VT_I4: _get_propvar_i4,
    VT_LPSTR: _get_propvar_lpstr,
    VT_FILETIME: _get_propvar_filetime,
    }

class _PROPVAR_VALUE(Structure):
    _fields_ = [('cElems', ULONG),
                ('pElems', LPVOID),
                ]

class PROPVARIANT(Structure):
    _fields_ = [('vt', VARTYPE),
                ('wReserved1', WORD),
                ('wReserved2', WORD),
                ('wReserved3', WORD),
                ('value', _PROPVAR_VALUE), # to avoid the huge union, cast a pointer to this field
                ]
    
    def get_value(self):
        try:
            getter = _propvar_getters[self.vt]
        except KeyError:
            return None
        else:
            return getter(self.value)

class STATPROPSTG(Structure):
    _fields_ = [('lpwstrName', LPOLESTR),
                ('propid', PROPID),
                ('vt', VARTYPE),
                ]

class IEnumSTATPROPSTG(IUnknown):
    iid = GUID("00000139-0000-0000-C000-000000000046")

    def __iter__(self):
        return self

    def __next__(self):
        result = STATPROPSTG()
        numfetched = ULONG()
        if self.Next(1, byref(result), byref(numfetched)) == S_OK:
            return result
        else:
            raise StopIteration

IEnumSTATPROPSTG.set_methods([('Next', HRESULT, ULONG, POINTER(STATPROPSTG), POINTER(ULONG)),
                              #RemoteNext = Next
                              ('Skip', HRESULT, ULONG),
                              ('Reset', HRESULT),
                              ('Clone', HRESULT, IEnumSTATPROPSTG.outparam),
                              ])

class STATPROPSETSTG(Structure):
    _fields_ = [('fmtid', FMTID),
                ('clsid', CLSID),
                ('grfFlags', DWORD),
                ('mtime', FILETIME),
                ('ctime', FILETIME),
                ('atime', FILETIME),
                ('dwOSVersion', DWORD),
                ]

class IPropertyStorage(IUnknown):
    iid = GUID("00000138-0000-0000-C000-000000000046")

    def __iter__(self):
        result = IEnumSTATPROPSTG.outparam()
        self.Enum(result)
        return result.get()

    def read_id(self, propid):
        spec = PROPSPEC()
        spec.ulKind = PRSPEC_PROPID
        spec.u.propid = propid
        result = PROPVARIANT()
        self.ReadMultiple(1, byref(spec), byref(result))
        return result

    com_methods = [('ReadMultiple', HRESULT, ULONG, POINTER(PROPSPEC), POINTER(PROPVARIANT)),
                   ('WriteMultiple', HRESULT, ULONG, POINTER(PROPSPEC), POINTER(PROPVARIANT), PROPID),
                   ('DeleteMultiple', HRESULT, ULONG, POINTER(PROPSPEC)),
                   ('ReadPropertyNames', HRESULT, ULONG, POINTER(PROPID), POINTER(LPOLESTR)),
                   ('WritePropertyNames', HRESULT, ULONG, POINTER(PROPID), POINTER(LPOLESTR)),
                   ('DeletePropertyNames', HRESULT, ULONG, POINTER(PROPID)),
                   ('Commit', HRESULT, DWORD),
                   ('Revert', HRESULT),
                   ('Enum', HRESULT, IEnumSTATPROPSTG.outparam),
                   ('SetTimes', HRESULT, POINTER(FILETIME), POINTER(FILETIME), POINTER(FILETIME)),
                   ('SetClass', HRESULT, REFCLSID),
                   ('Stat', HRESULT, POINTER(STATPROPSETSTG)),
                   ]

class IEnumSTATPROPSETSTG(IUnknown):
    iid = GUID("0000013B-0000-0000-C000-000000000046")

    def __iter__(self):
        return self

    def __next__(self):
        result = STATPROPSETSTG()
        numfetched = ULONG()
        if self.Next(1, byref(result), byref(numfetched)) == S_OK:
            return result
        else:
            raise StopIteration

    next = __next__

IEnumSTATPROPSETSTG.set_methods([('Next', HRESULT, ULONG, POINTER(STATPROPSETSTG), POINTER(ULONG)),
                                 #RemoteNext = Next
                                 ('Skip', HRESULT, ULONG),
                                 ('Reset', HRESULT),
                                 ('Clone', HRESULT, IEnumSTATPROPSETSTG.outparam),
                                 ])

class IPropertySetStorage(IUnknown):
    iid = GUID("0000013A-0000-0000-C000-000000000046")

    def __iter__(self):
        result = IEnumSTATPROPSETSTG.outparam()
        self.Enum(result)
        return result.get()

    com_methods = [('Create', HRESULT, REFFMTID, POINTER(CLSID), DWORD, DWORD, IPropertyStorage.outparam),
                   ('Open', HRESULT, REFFMTID, DWORD, IPropertyStorage.outparam),
                   ('Delete', HRESULT, REFFMTID),
                   ('Enum', HRESULT, IEnumSTATPROPSETSTG.outparam),
                   ]

class STGOPTIONS(Structure):
    _fields_ = [('usVersion', USHORT),
                ('reserved', USHORT),
                ('ulSectorSize', ULONG),
                ('pwcsTemplateFile', LPCWSTR),
                ]

_ole32 = windll.ole32

get_symbols(globals(), _ole32, ['ReadClassStg', 'StgCreateStorageEx', 'StgIsStorageFile', 'StgOpenStorage'])

ReadClassStg.restype = HRESULT
ReadClassStg.argtypes = [IStorage, POINTER(CLSID)]

StgCreateStorageEx.restype = HRESULT
StgCreateStorageEx.argtypes = [LPCWSTR, DWORD, STGFMT, DWORD, POINTER(STGOPTIONS), LPVOID, REFIID, POINTER(LPVOID)]

StgIsStorageFile.restype = HRESULT
StgIsStorageFile.argtypes = [LPCWSTR]

StgOpenStorage.restype = HRESULT
StgOpenStorage.argtypes = [LPCWSTR, IStorage, DWORD, SNB, DWORD, IStorage.outparam]

def is_storage_file(filename):
    try:
        return StgIsStorageFile(filename) == S_OK
    except WindowsError:
        return False

def open_storage(filename, mode):
    result = IStorage.outparam()
    StgOpenStorage(filename, None, mode, None, 0, result)
    return result.get()

_shlwapi = windll.shlwapi

get_aw_symbols(globals(), _shlwapi, ['SHCreateStreamOnFile'])

SHCreateStreamOnFile.argtypes = [LPCTSTR, DWORD, IStream.outparam]
SHCreateStreamOnFile.restype = HRESULT

def file_stream(filename, mode):
    result = IStream.outparam()
    SHCreateStreamOnFile(filename, mode, result)
    return result.get()

