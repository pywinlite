#Copyright (c) 2008 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# Window Class definitions
# see http://msdn.microsoft.com/en-us/library/ms632596(VS.85).aspx

try:
    import _thread
except ImportError:
    import thread as _thread # this module was renamed between 2.x and 3.x
import random

from ctypes import Structure, WINFUNCTYPE, POINTER, windll, byref, sizeof
from windef import UINT, INT, LONG, WORD, DWORD, HINSTANCE, HICON, HCURSOR, HBRUSH, HMENU, HWND, LPTSTR, LPCTSTR, LPVOID, LRESULT, WPARAM, LPARAM, create_tchar_buffer, BOOL
from winlitecfg import get_aw_symbols
from winliteutils import NONZERO
_user32 = windll.user32

CS_VREDRAW = 0x00000001
CS_HREDRAW = 0x00000002
CS_KEYCVTWINDOW = 0x00000004
CS_DBLCLKS = 0x00000008
CS_OWNDC = 0x00000020
CS_CLASSDC = 0x00000040
CS_PARENTDC = 0x00000080
CS_NOKEYCVT = 0x00000100
CS_NOCLOSE = 0x00000200
CS_SAVEBITS = 0x00000800
CS_BYTEALIGNCLIENT = 0x00001000
CS_BYTEALIGNWINDOW = 0x00002000
CS_GLOBALCLASS = 0x00004000
CS_IME = 0x00010000
CS_DROPSHADOW = 0x00020000

GCL_MENUNAME = -8
GCL_HBRBACKGROUND = -10
GCL_HCURSOR = -12
GCL_HICON = -14
GCL_HMODULE = -16
GCL_WNDPROC = -24
GCL_HICONSM = -34
GCL_CBWNDEXTRA = -18
GCL_CBCLSEXTRA = -20
GCL_STYLE = -26

GCLP_MENUNAME = -8
GCLP_HBRBACKGROUND = -10
GCLP_HCURSOR = -12
GCLP_HICON = -14
GCLP_HMODULE = -16
GCLP_WNDPROC = -24
GCLP_HICONSM = -34

GWL_EXSTYLE = -20
GWL_STYLE = -16
GWL_USERDATA = -21
GWL_ID = -12
GWL_HWNDPARENT = -8
GWL_HINSTANCE = -6
GWL_WNDPROC = -4
DWL_MSGRESULT = 0
DWL_DLGPROC = 4
DWL_USER = 8

# special COLOR_* values copied here to avoid importing brushes
COLOR_SCROLLBAR = 0
COLOR_BACKGROUND = 1
COLOR_ACTIVECAPTION = 2
COLOR_INACTIVECAPTION = 3
COLOR_MENU = 4
COLOR_WINDOW = 5
COLOR_WINDOWFRAME = 6
COLOR_MENUTEXT = 7
COLOR_WINDOWTEXT = 8
COLOR_CAPTIONTEXT = 9
COLOR_ACTIVEBORDER = 10
COLOR_INACTIVEBORDER = 11
COLOR_APPWORKSPACE = 12
COLOR_HIGHLIGHT = 13
COLOR_HIGHLIGHTTEXT = 14
COLOR_BTNFACE = 15
COLOR_BTNSHADOW = 16
COLOR_GRAYTEXT = 17
COLOR_BTNTEXT = 18
COLOR_INACTIVECAPTIONTEXT = 19
COLOR_BTNHIGHLIGHT = 20

CW_USEDEFAULT = 0x80000000

WS_OVERLAPPED = 0x00000000
WS_POPUP = 0x80000000
WS_CHILD = 0x40000000
WS_MINIMIZE = 0x20000000
WS_VISIBLE = 0x10000000
WS_DISABLED = 0x08000000
WS_CLIPSIBLINGS = 0x04000000
WS_CLIPCHILDREN = 0x02000000
WS_MAXIMIZE = 0x01000000
WS_CAPTION = 0x00C00000
WS_BORDER = 0x00800000
WS_DLGFRAME = 0x00400000
WS_VSCROLL = 0x00200000
WS_HSCROLL = 0x00100000
WS_SYSMENU = 0x00080000
WS_THICKFRAME = 0x00040000
WS_GROUP = 0x00020000
WS_TABSTOP = 0x00010000
WS_MINIMIZEBOX = 0x00020000
WS_MAXIMIZEBOX = 0x00010000
WS_TILED = WS_OVERLAPPED
WS_ICONIC = WS_MINIMIZE
WS_SIZEBOX = WS_THICKFRAME
WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME| WS_MINIMIZEBOX | WS_MAXIMIZEBOX
WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU
WS_CHILDWINDOW = WS_CHILD
WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW

WS_EX_DLGMODALFRAME = 0x00000001
WS_EX_DRAGDETECT = 0x00000002
WS_EX_NOPARENTNOTIFY = 0x00000004
WS_EX_TOPMOST = 0x00000008
WS_EX_ACCEPTFILES = 0x00000010
WS_EX_TRANSPARENT = 0x00000020
WS_EX_MDICHILD = 0x00000040
WS_EX_TOOLWINDOW = 0x00000080
WS_EX_WINDOWEDGE = 0x00000100
WS_EX_CLIENTEDGE = 0x00000200
WS_EX_CONTEXTHELP = 0x00000400
WS_EX_RIGHT = 0x00001000
WS_EX_LEFT = 0x00000000
WS_EX_RTLREADING = 0x00002000
WS_EX_LTRREADING = 0x00000000
WS_EX_LEFTSCROLLBAR = 0x00004000
WS_EX_RIGHTSCROLLBAR = 0x00000000
WS_EX_CONTROLPARENT = 0x00010000
WS_EX_STATICEDGE = 0x00020000
WS_EX_APPWINDOW = 0x00040000
WS_EX_LAYERED = 0x00080000
WS_EX_NOINHERITLAYOUT = 0x00100000
WS_EX_LAYOUTRTL = 0x00400000
WS_EX_COMPOSITED = 0x02000000
WS_EX_NOACTIVATE = 0x08000000

WS_EX_OVERLAPPEDWINDOW = (WS_EX_WINDOWEDGE|WS_EX_CLIENTEDGE)
WS_EX_PALETTEWINDOW = (WS_EX_WINDOWEDGE|WS_EX_TOOLWINDOW|WS_EX_TOPMOST)

SW_HIDE = 0
SW_SHOWNORMAL = 1
SW_NORMAL = 1
SW_SHOWMINIMIZED = 2
SW_SHOWMAXIMIZED = 3
SW_MAXIMIZE = 3
SW_SHOWNOACTIVATE = 4
SW_SHOW = 5
SW_MINIMIZE = 6
SW_SHOWMINNOACTIVE = 7
SW_SHOWNA = 8
SW_RESTORE = 9
SW_SHOWDEFAULT = 10
SW_FORCEMINIMIZE = 11
SW_MAX = 11

WNDPROC = WINFUNCTYPE(LRESULT, HWND, UINT, WPARAM, LPARAM)

class WNDCLASS(Structure):
    _fields_ = [
        ('style', UINT),
        ('lpfnWndProc', WNDPROC),
        ('cbClsExtra', INT),
        ('cbWndExtra', INT),
        ('hInstance', HINSTANCE),
        ('hIcon', HICON),
        ('hCursor', HCURSOR),
        ('hbrBackground', HBRUSH),
        ('lpszMenuName', LPCTSTR),
        ('lpszClassName', LPCTSTR),
        ]
LPWNDCLASS = PWNDCLASS = POINTER(WNDCLASS)

class WNDCLASSEX(Structure):
    _fields_ = [
        ('cbSize', UINT),
        ('style', UINT),
        ('lpfnWndProc', WNDPROC),
        ('cbClsExtra', INT),
        ('cbWndExtra', INT),
        ('hInstance', HINSTANCE),
        ('hIcon', HICON),
        ('hCursor', HCURSOR),
        ('hbrBackground', HBRUSH),
        ('lpszMenuName', LPCTSTR),
        ('lpszClassName', LPCTSTR),
        ('hIconSm', HICON),
        ]
LPWNDCLASSEX = PWNDCLASSEX = POINTER(WNDCLASSEX)

get_aw_symbols(globals(), _user32, ['CallWindowProc', 'DefWindowProc', 'GetClassInfo', 'GetClassInfoEx', 'GetClassLong', 'GetClassName', 'GetWindowLong', 'RegisterClass', 'RegisterClassEx', 'SetClassLong', 'SetWindowLong', 'SetClassLong', 'UnregisterClass', 'CreateWindowEx'])

CallWindowProc.argtypes = [WNDPROC,HWND,UINT,WPARAM,LPARAM]

DefWindowProc.argtypes = [HWND,UINT,WPARAM,LPARAM]

_GetClassInfo = GetClassInfo
_GetClassInfo.argtypes = [HINSTANCE, LPCTSTR, LPWNDCLASS]
_GetClassInfo.restype = NONZERO
def GetClassInfo(instance, classname):
    result = WNDCLASS()
    _GetClassInfo(instance, classname, byref(result))
    return result

_GetClassInfoEx = GetClassInfoEx
_GetClassInfoEx.argtypes = [HINSTANCE, LPCTSTR, LPWNDCLASSEX]
_GetClassInfoEx.restype = NONZERO
def GetClassInfoEx(instance, classname):
    result = WNDCLASSEX()
    _GetClassInfoEx(instance, classname, byref(result))
    return result

GetClassLong.argtypes = [HWND, INT]
GetClassLong.restype = NONZERO

GetClassLongPtr = GetClassLong

_GetClassName = GetClassName
_GetClassName.argtypes = [HWND, LPTSTR, INT]
_GetClassName.restype = NONZERO
def GetClassName(hwnd, maxlength=256):
    result = create_tchar_buffer(maxlength)
    _GetClassName(hwnd, byref(result), len(result))
    return result.value

GetClassWord = _user32.GetClassWord
GetClassWord.argtypes = [HWND, INT]
GetClassWord.restype = NONZERO

GetWindowLong.argtypes = [HWND, INT]
GetWindowLong.restype = NONZERO

GetWindowLongPtr = GetWindowLong

RegisterClass.argtypes = [LPWNDCLASS]
RegisterClass.restype = NONZERO

RegisterClassEx.argtypes = [LPWNDCLASSEX]
RegisterClassEx.restype = NONZERO

SetClassLong.argtypes = [HWND,INT,LONG]
SetClassLong.restype = NONZERO

SetClassLongPtr = SetClassLong

SetClassWord = _user32.SetClassWord
SetClassWord.argtypes = [HWND,INT,WORD]
SetClassWord.restype = NONZERO

SetWindowLong.argtypes = [HWND,INT,LONG]
SetWindowLong.restype = NONZERO

SetWindowLongPtr = SetWindowLong

UnregisterClass.argtypes = [LPCTSTR,HINSTANCE]
UnregisterClass.restype = NONZERO

CreateWindowEx.argtypes = [DWORD,LPCTSTR,LPCTSTR,DWORD,INT,INT,INT,INT,HWND,HMENU,HINSTANCE,LPVOID]
CreateWindowEx.restype = NONZERO

GetDesktopWindow = _user32.GetDesktopWindow
GetDesktopWindow.argtypes = []
GetDesktopWindow.restype = NONZERO

ShowWindow = _user32.ShowWindow
ShowWindow.argtypes = [HWND, INT]

MoveWindow = _user32.MoveWindow
MoveWindow.argtypes = [HWND, INT, INT, INT, INT, BOOL]
MoveWindow.restype = NONZERO

window_instances = {}
window_instance_lock = _thread.allocate_lock()

class WindowClass(type):
    """WindowClass - the type of all window classes

Don't use WindowClass directly. Subclass Window instead."""
    
    classcount = 0
    classcount_lock = _thread.allocate_lock()
    
    def __init__(self, name, bases, dict):
        if bases[0] is BaseWindow or bases[0] is _WindowMetaClassInstanceBase:
            #WindowClass is an abstract base type. None of the magic
            #things we do to initialize a real window type should be
            #done for WindowClass
            return type.__init__(self, name, bases, dict)
        
        #if internal is undefined, default to True
        if 'internal' not in dict:
            self.internal = True
        
        if 'subclass' not in dict:
            #if the superclass has the same name as this class, this is a subclass
            self.subclass = bases[0].classname is not None and bases[0].classname != self.classname
        
        self._superclass_wndproc = bases[0]._wndproc_ptr
        
        self.message_handlers = {}
        for name in dict:
            if name.startswith('DO_'):
                msgname = name[3:]
                if msgname.startswith('0x'):
                    msg = int(msgname[2:], 16)
                elif msgname.startswith('R_'):
                    import message
                    msg = message.RegisterWindowMessage(msgname[2:])
                else:
                    try:
                        msg = int(msgname)
                    except ValueError:
                        msg = getattr(__import__('message'), msgname)
                self.message_handlers[msg] = dict[name]
        
        type.__init__(self, name, bases, dict)
        
        if self.internal or self.subclass:
            self._wndproc_ptr = WNDPROC(self._wndproc)
        else:
            info = GetClassInfoEx(0, self.classname)
            self._wndproc_ptr = info.lpfnWndProc
            self._class_extrabytes = info.cbClsExtra
            self._wnd_extrabytes = info.cbWndExtra
            #self._class_instance = info.hInstance
            self.icon = info.hIcon
            self.cursor = info.hCursor
            if info.hbrBackground and info.hbrBackground <= COLOR_BTNHIGHLIGHT + 1:
                self.background = info.hbrBackground - 1
            else:
                self.background = info.hbrBackground
            self._class_menuname = info.lpszMenuName
            self.smallicon = info.hIconSm
        
        if self.internal and not self.subclass:
            if self.classname is None or self.classname == bases[0].classname:
                WindowClass.classcount_lock.acquire()
                self.classname = 'PyWinLite%s' % WindowClass.classcount
                WindowClass.classcount += 1
                WindowClass.classcount_lock.release()
            wce = WNDCLASSEX()
            wce.cbSize = sizeof(wce)
            wce.lpfnWndProc = self._wndproc_ptr
            wce.cbClsExtra = self._class_extrabytes
            wce.cbWndExtra = self._wnd_extrabytes
            wce.hInstance = self._class_instance
            wce.hIcon = self.icon
            if self.cursor is None:
                import cursor
                wce.hCursor = cursor.LoadCursor(0, cursor.IDC_ARROW)
            else:
                wce.hCursor = self.cursor
            if isinstance(self.background, int) and self.background <= 0xFFFF:
                wce.hbrBackground = self.background + 1
            else:
                wce.hbrBackground = self.background
            wce.lpszMenuName = self._class_menuname
            wce.lpszClassName = create_tchar_buffer(self.classname)
            wce.hIconSm = self.smallicon
            RegisterClassEx(byref(wce))
    
    def __call__(self, *args, **kwargs):
        hwnd = self.__create__(*args, **kwargs)
        result = window_from_hwnd(hwnd, self)
        result.__setup__(*args, **kwargs)
        return window_from_hwnd(hwnd)
    
    def __del__(self):
        pass # if internal, unregisterclass

class BaseWindow(object):
    def __init__(self):
        if type(self) is BaseWindow:
            raise TypeError("BaseWindow is an abstract base class")

class ExternalWindow(BaseWindow):
    pass

# Needed because 3.0 added new syntax for metaclasses and removed the old syntax.
_WindowMetaClassInstanceBase = WindowClass('_WindowMetaClassBase', (BaseWindow,), {})

class Window(_WindowMetaClassInstanceBase):
    classname = None
    internal = False
    subclass = False
    _superclass_wndproc = None
    
    #class creation settings
    _class_extrabytes = 0
    _wnd_extrabytes = 0
    _class_instance = 0
    icon = 0
    cursor = None # None is a special value that loads a default cursor; use 0 for no cursor
    background = COLOR_WINDOW
    _class_menuname = None
    smallicon = 0
    
    #instance creation settings
    style = WS_TILEDWINDOW
    exstyle = 0
    windowname = "PyWinLite"
    x = y = width = height = CW_USEDEFAULT
    parent = None
    menu = 0
    instance = 0
    lparam = 0
    
    @classmethod
    def super_on_msg(klass, hwnd, msg, wparam, lparam):
        if klass._superclass_wndproc is None:
            return DefWindowProc(hwnd, msg, wparam, lparam)
        else:
            return CallWindowProc(klass._superclass_wndproc, hwnd, msg, wparam, lparam)
    
    @classmethod
    def _wndproc(klass, hwnd, msg, wparam, lparam):
        wnd = window_from_hwnd(hwnd, klass)
        try:
            result = klass.on_msg(wnd, hwnd, msg, wparam, lparam)
            if result == None:
                return 0
            else:
                return result
        except NotImplementedError:
            return klass.super_on_msg(hwnd, msg, wparam, lparam)
    
    _wndproc_ptr = None
    
    def _on_msg(klass, wnd, hwnd, msg, wparam, lparam):
        try:
            handler = klass.message_handlers[msg]
        except KeyError:
            raise NotImplementedError
        return handler(wnd, hwnd, msg, wparam, lparam)
    
    on_msg = classmethod(_on_msg)
    _on_msg = staticmethod(_on_msg)
    
    @classmethod
    def __create__(klass, exstyle=None, classname=None, windowname=None, style=None, x=None, y=None, width=None, height=None, parent=None, menu=None, instance=None, lparam=None):
        """__create__(klass, exstyle=None, classname=None, windowname=None, style=None, x=None, y=None, width=None, height=None, parent=None, menu=None, instance=None, lparam=None)
This function calls CreateWindowEx and returns an hwnd. It is called with the arguments used to create the instance."""
        if exstyle is None: exstyle = klass.exstyle
        if classname is None: classname = klass.classname
        if windowname is None: windowname = klass.windowname
        if style is None: style = klass.style
        if x is None: x = klass.x
        if y is None: y = klass.y
        if width is None: width = klass.width
        if height is None: height = klass.height
        if parent is None: parent = klass.parent
        if menu is None: menu = klass.menu
        if instance is None: instance = klass.instance
        if lparam is None: lparam = klass.lparam
        return CreateWindowEx(exstyle, classname, windowname, style, x, y, width, height, parent, menu, instance, lparam)
    
    def __setup__(self, *args, **kwargs):
        """__setup__(self, *args, **kwargs)
Called after CreateWindowEx returns with arguments used to create the instance. Note that, because of how CreateWindow works, the instance will have already processed some messages by this time."""
    
    def __init__(self, hwnd):
        """__init__(self, hwnd)
Called before processing the first message, always with the hwnd of this window. The arguments used to create the instance cannot be automatically forwarded to this function. If you REALLY need them now, use lparam. Otherwise, process them in __setup__."""
        BaseWindow.__init__(self)

        if type(self) is Window:
            raise TypeError("Window is an abstract base class")

        self._as_parameter_ = self.hwnd = hwnd
        
        if self.parent is None:
            self.parent = GetDesktopWindow()

        if type(self).subclass:
            SetWindowLongPtr(hwnd, GWLP_WNDPROC, type(self)._wndproc_ptr)
    
    @classmethod
    def from_hwnd(klass, hwnd):
        result = klass.__new__(klass, hwnd)
        result.__init__(hwnd)
        return result
    
    def __del__(self):
        pass #DestroyWindow(self.hwnd)

def window_from_hwnd(hwnd, klass=ExternalWindow):
    try:
        return window_instances[hwnd]
    except KeyError:
        window_instance_lock.acquire()
        try:
            return window_instances[hwnd]
        except KeyError:
            window_instances[hwnd] = klass.from_hwnd(hwnd) #TODO: implement from_hwnd for ExternalWindow
            return window_instances[hwnd]
        finally:
            window_instance_lock.release()

