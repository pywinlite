#Copyright (c) 2008 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# Version Information definitions
# see http://msdn.microsoft.com/en-us/library/ms646981(VS.85).aspx

from ctypes import windll, byref, create_string_buffer
from winlitecfg import get_aw_symbols
from winliteutils import NONZERO, get_symbols, ERRFLAGS
from windef import LPCTSTR, DWORD, LPVOID, LPCTSTR, LPCSTR, LPTSTR, PUINT, MAX_PATH, create_tchar_buffer, LPDWORD, LPCVOID, UINT

_version = windll.version
_kernel32 = windll.kernel32

FILE_VER_GET_LOCALISED = 1
FILE_VER_GET_NEUTRAL = 2

VIF_TEMPFILE = 0x00000001
VIF_MISMATCH = 0x00000002
VIF_SRCOLD = 0x00000004
VIF_DIFFLANG = 0x00000008
VIF_DIFFCODEPG = 0x00000010
VIF_DIFFTYPE = 0x00000020
VIF_WRITEPROT = 0x00000040
VIF_FILEINUSE = 0x00000080
VIF_OUTOFSPACE = 0x00000100
VIF_ACCESSVIOLATION = 0x00000200
VIF_SHARINGVIOLATION = 0x00000400
VIF_CANNOTCREATE = 0x00000800
VIF_CANNOTDELETE = 0x00001000
VIF_CANNOTRENAME = 0x00002000
VIF_CANNOTDELETECUR = 0x00004000
VIF_OUTOFMEMORY = 0x00008000
VIF_CANNOTREADSRC = 0x00010000
VIF_CANNOTREADDST = 0x00020000
VIF_BUFFTOOSMALL = 0x00040000

VIFF_FORCEINSTALL = 1
VIFF_DONTDELETEOLD = 1

VFF_CURNEDEST = 1
VFF_FILEINUSE = 2
VFF_BUFFTOOSMALL = 4

VFFF_ISSHAREDFILE = 1

get_aw_symbols(globals(), _version, ['GetFileVersionInfo', 'GetFileVersionInfoSize', 'VerFindFile', 'VerInstallFile', 'VerQueryValue'])

GetFileVersionInfoSize.argtypes = [LPCTSTR, LPDWORD]
GetFileVersionInfoSize.restype = NONZERO
_GetFileVersionInfoSize = GetFileVersionInfoSize
def GetFileVersionInfoSize(filename):
    handle = DWORD()
    return _GetFileVersionInfoSize(filename, byref(handle))

GetFileVersionInfo.argtypes = [LPCTSTR, DWORD, DWORD, LPVOID]
GetFileVersionInfo.restype = NONZERO
_GetFileVersionInfo = GetFileVersionInfo
def GetFileVersionInfo(filename, bytes=None):
    if bytes is None:
        bytes = GetFileVersionInfoSize(filename)
    result = create_string_buffer(bytes)
    _GetFileVersionInfo(filename, 0, bytes, result)
    return result

VerFindFile.argtypes = [DWORD, LPCTSTR, LPCTSTR, LPCTSTR, LPCSTR, PUINT, LPTSTR, PUINT]
_VerFindFile = VerFindFile
def VerFindFile(flags, filename, windir, appdir, curdirlen=MAX_PATH, destdirlen=MAX_PATH):
    curdir = create_string_buffer(curdirlen)
    destdir = create_tchar_buffer(destdirlen)
    curdirlen = UINT(curdirlen)
    destdirlen = UINT(destdirlen)
    ret = _VerFindFile(flags, filename, windir, appdir, curdir, byref(curdirlen), destdir, byref(destdirlen))
    return ret, curdir.value, destdir.value

VerInstallFile.argtypes = [DWORD, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPCTSTR, LPTSTR, PUINT]
VerInstallFile.restype = ERRFLAGS(globals(), 'VIF_')
_VerInstallFile = VerInstallFile
def VerInstallFile(flags, srcfilename, destfilename, srcdir, destdir, curdir, tmpfilelen=MAX_PATH):
    tmpfile = create_tchar_buffer(tmpfilelen)
    tmpfilelen = UINT(tmpfilelen)
    _VerInstallFile(flags, srcfilename, destfilename, srcdir, destdir, curdir, tmpfile, byref(tmpfilelen))
    return tmpfile.value

VerQueryValue.argtypes = [LPCVOID, LPCTSTR, LPVOID, PUINT]
_VerQueryValue = VerQueryValue
def VerQueryValue(block, subblock):
    ppbuffer = LPVOID()
    len = UINT()
    _VerQueryValue(block, subblock, byref(ppbuffer), byref(len))
    return ppbuffer.value, len.value

get_aw_symbols(globals(), _kernel32, ['VerLanguageName'])

VerLanguageName.argtypes = [DWORD, LPTSTR, DWORD]
VerLanguageName.restype = NONZERO
_VerLanguageName = VerLanguageName
def VerLanguageName(langid, langsize=None):
    if langsize is None:
        langsize = _VerLanguageName(langid, 0, 0)
    lang = create_tchar_buffer(langsize)
    _VerLanguageName(langid, lang, langsize)
    return lang.value

try:
    #requires windows vista
    get_symbols(globals(), _version, ['GetFileVersionInfoEx', 'GetFileVersionInfoSizeEx'])
except AttributeError:
    pass
else:
    GetFileVersionInfoSizeEx.argtypes = [DWORD, LPCTSTR, LPDWORD]
    GetFileVersionInfoSizeEx.restype = NONZERO
    _GetFileVersionInfoSizeEx = GetFileVersionInfoSizeEx
    def GetFileVersionInfoSizeEx(flags, filename):
        handle = DWORD()
        return _GetFileVersionInfoSizeEx(flags, filename, byref(handle))
    
    GetFileVersionInfoEx.argtypes = [DWORD, LPCTSTR, DWORD, DWORD, LPVOID]
    GetFileVersionInfoEx.restype = NONZERO
    _GetFileVersionInfoEx = GetFileVersionInfoEx
    def GetFileVersionInfoEx(flags, filename, bytes=None):
        if bytes is None:
            bytes = GetFileVersionInfoSizeEx(flags, filename)
        result = create_string_buffer(bytes)
        _GetFileVersionInfoEx(flags, filename, 0, bytes, result)
        return result

