#Copyright (c) 2008 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# Cursor definitions
# see http://msdn.microsoft.com/en-us/library/ms646970(VS.85).aspx

from ctypes import windll, Structure, POINTER, sizeof, byref
from windef import LPRECT, HCURSOR, HINSTANCE, INT, LPVOID, RECT, DWORD, POINT, LPPOINT, LPCTSTR, BOOL, HICON
from winlitecfg import get_aw_symbols
from winliteutils import NONZERO
from resource import MAKEINTRESOURCE
_user32 = windll.user32

IDC_ARROW = MAKEINTRESOURCE(32512)
IDC_IBEAM = MAKEINTRESOURCE(32513)
IDC_WAIT = MAKEINTRESOURCE(32514)
IDC_CROSS = MAKEINTRESOURCE(32515)
IDC_UPARROW = MAKEINTRESOURCE(32516)
IDC_SIZE = MAKEINTRESOURCE(32640)
IDC_ICON = MAKEINTRESOURCE(32641)
IDC_SIZENWSE = MAKEINTRESOURCE(32642)
IDC_SIZENESW = MAKEINTRESOURCE(32643)
IDC_SIZEWE = MAKEINTRESOURCE(32644)
IDC_SIZENS = MAKEINTRESOURCE(32645)
IDC_SIZEALL = MAKEINTRESOURCE(32646)
IDC_NO = MAKEINTRESOURCE(32648)
IDC_HAND = MAKEINTRESOURCE(32649)
IDC_APPSTARTING = MAKEINTRESOURCE(32650)
IDC_HELP = MAKEINTRESOURCE(32651)

OCR_NORMAL = 32512
OCR_IBEAM = 32513
OCR_WAIT = 32514
OCR_CROSS = 32515
OCR_UP = 32516
OCR_SIZE = 32640
OCR_ICON = 32641
OCR_SIZENWSE = 32642
OCR_SIZENESW = 32643
OCR_SIZEWE = 32644
OCR_SIZENS = 32645
OCR_SIZEALL = 32646
OCR_ICOCUR = 32647
OCR_NO = 32648
OCR_HAND = 32649
OCR_APPSTARTING = 32650
OCR_HELP = 32651

OCR_DRAGOBJECT = 32653 #defined in wine's winuser.h

class CURSORINFO(Structure):
    _fields_ = [
        ('cbSize', DWORD),
        ('flags', DWORD),
        ('hCursor', HCURSOR),
        ('ptScreenPos', POINT),
        ]
PCURSORINFO = LPCURSORINFO = POINTER(CURSORINFO)

get_aw_symbols(globals(), _user32, ['LoadCursor', 'LoadCursorFromFile'])

_user32.ClipCursor.argtypes = [LPRECT]
_user32.ClipCursor.restype = NONZERO
ClipCursor = _user32.ClipCursor

_user32.CopyIcon.argtypes = [HICON]
_user32.CopyIcon.restype = NONZERO
CopyCursor = _user32.CopyIcon

_user32.CreateCursor.argtypes = [HINSTANCE, INT, INT, INT, INT, LPVOID, LPVOID]
_user32.CreateCursor.restype = NONZERO
CreateCursor = _user32.CreateCursor

_user32.DestroyCursor.argtypes = [HCURSOR]
_user32.DestroyCursor.restype = NONZERO
DestroyCursor = _user32.DestroyCursor

_user32.GetClipCursor.argtypes = [LPRECT]
_user32.GetClipCursor.restype = NONZERO
def GetClipCursor():
    result = RECT()
    _user32.GetClipCursor(byref(result))
    return result

_user32.GetCursor.argtypes = []
_user32.GetCursor.restype = HCURSOR
GetCursor = _user32.GetCursor

_user32.GetCursorInfo.argtypes = [PCURSORINFO]
_user32.GetCursorInfo.restype = NONZERO
def GetCursorInfo():
    result = CURSORINFO()
    result.cbSize = sizeof(result)
    _user32.GetCursorInfo(byref(result))
    return result

_user32.GetCursorPos.argtypes = [LPPOINT]
_user32.GetCursorPos.restype = NONZERO
def GetCursorPos():
    result = POINT()
    _user32.GetCursorPos(byref(result))
    return result

LoadCursor.argtypes = [HINSTANCE, LPCTSTR]
LoadCursor.restype = NONZERO

LoadCursorFromFile.argtypes = [LPCTSTR]
LoadCursorFromFile.restype = NONZERO

_user32.SetCursor.argtypes = [HCURSOR]
_user32.SetCursor.restype = HCURSOR
SetCursor = _user32.SetCursor

_user32.SetCursorPos.argtypes = [INT, INT]
_user32.SetCursorPos.restype = NONZERO
SetCursorPos = _user32.SetCursorPos

_user32.SetSystemCursor.argtypes = [HCURSOR, DWORD]
_user32.SetSystemCursor.restype = NONZERO
SetSystemCursor = _user32.SetSystemCursor

_user32.ShowCursor.argtypes = [BOOL]
_user32.ShowCursor.restype = INT
ShowCursor = _user32.ShowCursor

# functions in Windows Vista
try:
    _user32.GetPhysicalCursorPos.argtypes = [LPPOINT]
    _user32.GetPhysicalCursorPos.restype = NONZERO
    def GetPhysicalCursorPos():
        result = POINT()
        _user32.GetPhysicalCursorPos(byref(result))
        return result
    
    _user32.SetPhysicalCursorPos.argtypes = [INT, INT]
    _user32.SetPhysicalCursorPos.restype = NONZERO
    SetPhysicalCursorPos = _user32.SetPhysicalCursorPos
except AttributeError:
    pass

