#Copyright (c) 2009 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

from ctypes import windll, Structure, c_void_p, byref, POINTER, WINFUNCTYPE, cast, create_unicode_buffer, c_float

from windef import INT, ULONG_PTR, UINT32, BOOL, LPSTR, LPWSTR, LANGID, HDC, HANDLE, DWORD, CLSID, GUID, LPBYTE, UINT
from winliteutils import get_symbols

REAL = c_float

_gdiplus = windll.gdiplus

get_symbols(globals(), _gdiplus, ['GdipAddPathLine', 'GdipCreateFromHDC', 'GdipCreateFromHDC2', 'GdipCreatePath', 'GdipFillRectangle', 'GdipCreatePathGradientFromPath', 'GdipGetImageDecoders', 'GdipGetImageDecodersSize', 'GdipGetImageEncoders', 'GdipGetImageEncodersSize', 'GdipDeleteGraphics', 'GdipGetFamilyName', 'GdipGetFontCollectionFamilyCount', 'GdipGetFontCollectionFamilyList', 'GdipGraphicsClear', 'GdipNewInstalledFontCollection', 'GdipSetPathGradientCenterPoint', 'GdiplusShutdown', 'GdiplusStartup'])

class GdiplusError(Exception):
    pass

_gpstatus_descriptions = {
    1: "GenericError",
    2: "InvalidParameter",
    3: "OutOfMemory",
    4: "ObjectBusy",
    5: "InsufficientBuffer",
    6: "NotImplemented",
    7: "Win32Error",
    8: "WrongSize",
    9: "Aborted",
    10: "FileNotFound",
    11: "ValueOverflow",
    12: "AccessDenied",
    13: "UnknownImageFormat",
    14: "FontFamilyNotFound",
    15: "FontStyleNotFound",
    16: "NotTrueTypeFont",
    17: "UnsupportedGdiplusVersion",
    18: "GdiplusNotInitialized",
    19: "PropertyNotFound",
    20: "PropertyNotSupported",
    21: "ProfileNotFound",
    }

def GpStatus(n):
    if n:
        exc = GdiplusError(_gpstatus_descriptions[n])
        exc.id = n
        raise exc

DebugEventProc = WINFUNCTYPE(c_void_p, INT, LPSTR)

class GdiplusStartupInput(Structure):
    _fields_ = [('GdiplusVersion', UINT32),
                ('DebugEventCallback', DebugEventProc),
                ('SuppressBackgroundThread', BOOL),
                ('SuppressExternalCodecs', BOOL),
                ]

class GdiplusStartupOutput(Structure):
    _fields_ = [('NotificationHook', WINFUNCTYPE(GpStatus, POINTER(ULONG_PTR))),
                ('NotificationUnhook', WINFUNCTYPE(None, ULONG_PTR)),
                ]

class _GdiplusToken(object):
    def __init__(self):
        self.int_token = ULONG_PTR()
        startup = GdiplusStartupInput()
        startup.GdiplusVersion = 1
        startup.DebugEventCallback = cast(c_void_p(0), DebugEventProc)
        startup.SuppressBackgroundThread = 0
        startup.SuppressExternalCodecs = 0
        GdiplusStartup(byref(self.int_token), byref(startup), None)

    def __del__(self):
        import ctypes
        ctypes.windll.gdiplus.GdiplusShutdown(self.int_token)

GdiplusStartup.argtypes = [POINTER(ULONG_PTR), POINTER(GdiplusStartupInput), POINTER(GdiplusStartupOutput)]
GdiplusStartup.restype = GpStatus

GdiplusShutdown.argtypes = [ULONG_PTR]
GdiplusShutdown.restype = GpStatus

class ARGB(Structure):
    _fields_ = [('val', DWORD)]

    def __init__(self, red=0, green=0, blue=0, alpha=0xff):
        Structure.__init__(self)
        self.val = (alpha&0xff)<<24|(blue&0xff)<<16|(green&0xff)<<8|(red&0xff)

class GpPointF(Structure):
    _fields_ = [('X', REAL), ('Y', REAL)]

class GdiplusBase(Structure):
    _fields_ = [('ptr', c_void_p)]

FillModeAlternate = 0
FillModeWinding = 1
class GraphicsPath(GdiplusBase):
    def __init__(self, fillmode=FillModeAlternate, override=False):
        GdiplusBase.__init__(self)
        if not override:
            GdipCreatePath(fillmode, byref(self))

    def AddLine(self, x1, y1, x2, y2):
        GdipAddPathLine(self, x1, y1, x2, y2)

GdipAddPathLine.argtypes = [GraphicsPath, REAL, REAL, REAL, REAL]
GdipAddPathLine.restype = GpStatus

GdipCreatePath.argtypes = [INT, POINTER(GraphicsPath)]
GdipCreatePath.restype = GpStatus

class Brush(GdiplusBase):
    pass

class PathGradientBrush(Brush):
    @classmethod
    def from_path(cls, path):
        result = cls()
        GdipCreatePathGradientFromPath(path, result)
        return result

    def SetCenterPoint(self, point):
        GdipSetPathGradientCenterPoint(self, byref(point))

GdipCreatePathGradientFromPath.argtypes = [GraphicsPath, POINTER(PathGradientBrush)]
GdipCreatePathGradientFromPath.restype = GpStatus

GdipSetPathGradientCenterPoint.argtypes = [PathGradientBrush, POINTER(GpPointF)]
GdipSetPathGradientCenterPoint.restype = GpStatus 

class FontFamily(GdiplusBase):
    def GetFamilyName(self, langid=0):
        LF_FACESIZE = 32
        result = create_unicode_buffer(LF_FACESIZE)
        GdipGetFamilyName(self, result, langid)
        return result.value

GdipGetFamilyName.argtypes = [FontFamily, LPWSTR, LANGID]
GdipGetFamilyName.restype = GpStatus

class FontCollection(GdiplusBase):
    def GetFamilyCount(self):
        result = INT()
        GdipGetFontCollectionFamilyCount(self, byref(result))
        return result.value

    def __iter__(self):
        num_sought = self.GetFamilyCount()
        families = (FontFamily * num_sought)()
        num_found = INT()
        GdipGetFontCollectionFamilyList(self, num_sought, families, byref(num_found))
        for i in range(num_found.value):
            yield families[i]

GdipGetFontCollectionFamilyCount.argtypes = [FontCollection, POINTER(INT)]
GdipGetFontCollectionFamilyCount.restype = GpStatus

GdipGetFontCollectionFamilyList.argtypes = [FontCollection, INT, POINTER(FontFamily), POINTER(INT)]
GdipGetFontCollectionFamilyList.restype = GpStatus

class InstalledFontCollection(FontCollection):
    def __init__(self):
        FontCollection.__init__(self)
        GdipNewInstalledFontCollection(byref(self))

GdipNewInstalledFontCollection.argtypes = [POINTER(InstalledFontCollection)]
GdipNewInstalledFontCollection.restype = GpStatus

class ImageCodecInfo(Structure):
    _fields_ = [('Clsid', CLSID),
                ('FormatID', GUID),
                ('CodecName', LPWSTR),
                ('DllName', LPWSTR),
                ('FormatDescription', LPWSTR),
                ('FilenameExtension', LPWSTR),
                ('MimeType', LPWSTR),
                ('Flags', DWORD),
                ('Version', DWORD),
                ('SigCount', DWORD),
                ('SigSize', DWORD),
                ('SigPattern', LPBYTE),
                ('SigMask', LPBYTE),
                ]

GdipGetImageDecoders.argtypes = [UINT, UINT, POINTER(ImageCodecInfo)]
GdipGetImageDecoders.restype = GpStatus

GdipGetImageDecodersSize.argtypes = [POINTER(UINT), POINTER(UINT)]
GdipGetImageDecodersSize.restype = GpStatus

GdipGetImageEncoders.argtypes = [UINT, UINT, POINTER(ImageCodecInfo)]
GdipGetImageEncoders.restype = GpStatus

GdipGetImageEncodersSize.argtypes = [POINTER(UINT), POINTER(UINT)]
GdipGetImageEncodersSize.restype = GpStatus

class Graphics(GdiplusBase):
    @classmethod
    def from_hdc(cls, hdc, handle=None):
        result = cls()
        if handle == None:
            GdipCreateFromHDC(hdc, byref(result))
        else:
            GdipCreateFromHDC2(hdc, handle, byref(result))
        return result

    def Clear(self, color):
        GdipGraphicsClear(self, color)

    def FillRectangle(self, brush, x, y, width, height):
        GdipFillRectangle(self, brush, x, y, width, height)

    def __del__(self):
        GdipDeleteGraphics(self)

GdipCreateFromHDC.argtypes = [HDC, POINTER(Graphics)]
GdipCreateFromHDC.restype = GpStatus

GdipCreateFromHDC2.argtypes = [HDC, HANDLE, POINTER(Graphics)]
GdipCreateFromHDC2.restype = GpStatus

GdipDeleteGraphics.argtypes = [Graphics]
GdipDeleteGraphics.restype = GpStatus

GdipFillRectangle.argtypes = [Graphics, Brush, REAL, REAL, REAL, REAL]
GdipFillRectangle.restype = GpStatus

GdipGraphicsClear.argtypes = [Graphics, ARGB]
GdipGraphicsClear.restype = GpStatus

_token = _GdiplusToken()

