#Copyright (c) 2008 Vincent Povirk
#
#Permission is hereby granted, free of charge, to any person
#obtaining a copy of this software and associated documentation
#files (the "Software"), to deal in the Software without
#restriction, including without limitation the rights to use,
#copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the
#Software is furnished to do so, subject to the following
#conditions:
#
#The above copyright notice and this permission notice shall be
#included in all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#OTHER DEALINGS IN THE SOFTWARE.

# Caret definitions
# see http://msdn.microsoft.com/en-us/library/ms646968(VS.85).aspx

from ctypes import windll, byref
from windef import HWND, HBITMAP, INT, LPPOINT, POINT, UINT
from winliteutils import NONZERO
_user32 = windll.user32

_user32.CreateCaret.argtypes = [HWND, HBITMAP, INT, INT]
_user32.CreateCaret.restype = NONZERO
CreateCaret = _user32.CreateCaret

_user32.DestroyCaret.argtypes = []
_user32.DestroyCaret.restype = NONZERO
DestroyCaret = _user32.DestroyCaret

_user32.GetCaretBlinkTime.argtypes = []
_user32.GetCaretBlinkTime.restype = NONZERO
GetCaretBlinkTime = _user32.GetCaretBlinkTime

_user32.GetCaretPos.argtypes = [LPPOINT]
_user32.GetCaretPos.restype = NONZERO
def GetCaretPos():
    result = POINT()
    _user32.GetCaretPos(byref(result))
    return result

_user32.HideCaret.argtypes = [HWND]
_user32.HideCaret.restype = NONZERO
HideCaret = _user32.HideCaret

_user32.SetCaretBlinkTime.argtypes = [UINT]
_user32.SetCaretBlinkTime.restype = NONZERO
SetCaretBlinkTime = _user32.SetCaretBlinkTime

_user32.SetCaretPos.argtypes = [INT, INT]
_user32.SetCaretPos.restype = NONZERO
SetCaretPos = _user32.SetCaretPos

_user32.ShowCaret.argtypes = [HWND]
_user32.ShowCaret.restype = NONZERO
ShowCaret = _user32.ShowCaret

